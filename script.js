//  Créé un script JS permettant d'appeler l'API https://jsonplaceholder.typicode.com/users

// Affiche la liste des utilisateurs récupérés par cette API, ainsi que leur adresse complète, de la forme : nom, rue, code postal, ville
// Ajoute des commentaires afin d'expliquer ton code
// Versionne ton code et partage un lien vers un dépôt GitLab en solution

async function getUsers() {
    // Appel fetch de la data à l'API https://jsonplaceholder.typicode.com/users
  const response = await fetch("https://jsonplaceholder.typicode.com/users");
  // Stockage du corps de la réponse json dans une constante userList
  const userList = await response.json();

  // Boucle map pour afficher les informations souhaitées dans la console, pour chaque user du tableau json
  userList.map((user) => console.log({
    "nom" : user.name,
    "rue": user.address.street,
    "code postal": user.address.zipcode, 
    "ville": user.address.city
  }));
}

// Appel/déclenchement de la fonction qui génère le fetch
getUsers();
